(** This is an extension to the EasyCrypt's Array theory with several facts relating
 array distributions, samplings and loops.
*)

require import AllCore Real Distr Int List Array DList.
require (*--*) Bigop.

lemma size_ofarray (xs : 'x array) : size (ofarray xs) = size xs by smt().

lemma memE (x : 'x) xs:
  x \in ofarray xs <=> exists i, 0 <= i < size xs /\ x = xs.[i].
proof.
split.
+ move=> x_in_xs; exists (index x (ofarray xs)).
  rewrite getE nth_index //=.
  by rewrite index_ge0 sizeE index_mem x_in_xs.
by move=> /> i ge0_i ltsz_i; rewrite getE mem_nth 1:-sizeE.
qed.
    
(** Empty *)
op empty : 'x array = mkarray [] axiomatized by emptyE.

lemma size_empty : size (empty<:'x>) = 0. 
proof.
by rewrite emptyE size_mkarray size_eq0.
qed.
    
lemma empty_unique (xs:'x array):
  size xs = 0 => xs = empty.
proof. 
by move=> xs0; apply: arrayP; rewrite size_empty /#.
qed.

(** Append *)
op (||) (a1 a2 : 'x array) = mkarray (ofarray a1 ++ ofarray a2)
axiomatized by appendE.

lemma size_append (xs0 xs1:'x array):
  size (xs0 || xs1) = size xs0 + size xs1.
proof. 
by rewrite appendE size_mkarray size_cat ?size_ofarray.
qed.

lemma get_append (xs0 xs1:'x array) (i:int):
  0 <= i < size (xs0 || xs1) =>
  (xs0 || xs1).[i] = (0 <= i < size xs0) ? xs0.[i] : xs1.[i - size xs0].
proof. 
rewrite size_append; move => Hsize. 
rewrite ?getE.
rewrite appendE ofarrayK nth_cat.
case (0 <= i < size xs0) => hc.
+ by have ->: i < size (ofarray xs0) <=> true by smt().
have ->: i < size (ofarray xs0) <=> false by smt().
by rewrite size_ofarray.
qed.  

lemma nosmt get_append_left (xs0 xs1:'x array) (i:int):
 0 <= i < size xs0 =>
 (xs0 || xs1).[i] = xs0.[i].
proof.
move=> /> ge0_i gti_size_xs0.
by rewrite get_append 1:size_append; smt(size_ge0).
qed.

lemma nosmt get_append_right (xs0 xs1:'x array) (i:int):
 size xs0 <= i < size xs0 + size xs1 =>
 (xs0 || xs1).[i] = xs1.[i - size xs0].
proof.
move=> /> ge0_i lti_size.
by rewrite get_append 1:size_append; smt(size_ge0).
qed.

lemma set_append (xs0 xs1:'x array) (i:int) x:
  0 <= i < size (xs0 || xs1) =>
  (xs0 || xs1).[i <- x] = (0 <= i < size xs0) ? 
     (xs0.[i <- x] || xs1) : (xs0 || xs1.[i - size xs0 <- x]).
proof. 
move => [ge0_i]; rewrite size_append => gi_size.  
case (i < size xs0) => hcase.
+ rewrite ge0_i; simplify.
  apply arrayP; split; first by smt(size_append size_set).
  by move => k hk; smt(size_set size_append get_set get_append).
apply: arrayP=> //=; split; first by smt(size_append size_set).
rewrite size_set size_append; move => k hk.
rewrite get_set; first by rewrite size_append.
case (k=i) => hc; subst.
+ rewrite get_append; first by rewrite size_append size_set. 
  have ->: !(0 <= i < size xs0) by smt().
  by rewrite get_set=> //#.
smt(size_append size_set get_append get_set).
qed.

lemma nosmt set_append_left (xs0 xs1:'x array) (i:int) v:
 0 <= i < size xs0 =>
 (xs0 || xs1).[i <- v] = (xs0.[i <- v] || xs1).
proof.
move=> /> ge0_i lti_size_xs0.
by rewrite set_append 1:size_append; smt(size_ge0).
qed.

lemma nosmt set_append_right (xs0 xs1:'x array) (i:int) v:
 size xs0 <= i < size xs0 + size xs1 =>
 (xs0 || xs1).[i <- v] = (xs0 || xs1.[i - size xs0 <- v]).
proof.
move=> /> ge0_i lti_size.
by rewrite set_append 1:size_append; smt(size_ge0).
qed.

lemma make_append (a:'a) (l1 l2:int):
  0 <= l1 => 0 <= l2 => offun (fun k, a) (l1 + l2) = (offun (fun k, a) l1 || offun (fun k, a) l2).
proof.
move=> le0_l1 le0_l2.
apply arrayP; split; first by rewrite size_append ?size_offun // => /#. 
move => i Hisize.
rewrite get_append; first smt(size_append size_offun).
case (0 <= i < size (offun (fun (_ : int) => a) l1)) => hc. 
+ rewrite ?offunifE.
  have ->: 0 <= i < l1 + l2 <=> true by smt(size_offun).
  by have ->: 0 <= i < l1 <=> true by smt(size_offun).
rewrite ?offunifE.
have ->: 0 <= i < l1 + l2 <=> true by smt(size_offun).
have ->: 0 <= i - size (offun (fun (_ : int) => a) l1) < l2 <=> true.
+ split; first by smt(size_offun).
  move => hi.      
  by rewrite size_offun #smt:(size_offun).
by move: Hisize; rewrite size_offun /#.
qed.

(** mapi *)
op mapi: (int -> 'x -> 'y) -> 'x array -> 'y array.

axiom size_mapi (f:int -> 'x -> 'y) (xs:'x array):
  size (mapi f xs) = size xs.

axiom get_mapi (f:int -> 'x -> 'y) (xs:'x array) (i:int):
  0 <= i < size xs =>
  (mapi f xs).[i] = f i (xs.[i]).

lemma mapi_mapi (f:int -> 'x -> 'y) (g:int -> 'y -> 'z) xs:
 mapi g (mapi f xs) = mapi (fun k x, g k (f k x)) xs.
proof.
apply arrayP; split.
+ by rewrite !size_mapi.
by move=> i; rewrite !size_mapi=> i_bnd; rewrite ?get_mapi ?size_mapi.
qed.

lemma mapi_id (f:int -> 'x -> 'x) (xs:'x array):
 (forall k x, 0 <= k < size xs => f k x = x) =>
 mapi f xs = xs.
proof.
move=> f_id; apply arrayP; split.
+ by rewrite size_mapi.
by move=> i; rewrite !size_mapi=> i_bnd; rewrite ?get_mapi ?length_mapi ?f_id.
qed.

(** take *)
op take (l:int) (xs:'a array) = mkarray (take l (ofarray xs)) axiomatized by takeE.

lemma size_take (xs:'a array) (l:int):
  0 <= l <= size xs =>
  size (take l xs) = l.
proof. by rewrite takeE size_mkarray; smt(size_take). qed.

lemma get_take (xs:'a array) (l k:int):
  0 <= k < l <= size xs =>
  (take l xs).[k] = xs.[k].
proof.
move => Hksize.
by rewrite ?getE takeE ofarrayK; smt(nth_take).
qed.

(* drop *)
op drop (l:int) (xs:'a array) = mkarray (drop l (ofarray xs)) axiomatized by dropE.

lemma size_drop (xs:'a array) (l:int):
  0 <= l <= size xs =>
  size (drop l xs) = size xs - l.
proof.
move => [ge0_h gel_size].
rewrite dropE size_mkarray size_drop; first by exact ge0_h.
by rewrite size_ofarray; first by smt().
qed.
    
lemma get_drop  (xs:'a array) (l k:int):
  0 <= l => 0 <= k < size xs-l =>
  (drop l xs).[k] = xs.[l + k].
proof.
move => Hl Hksize.
by rewrite ?getE dropE ofarrayK; smt(nth_drop).
qed.

lemma take_drop (xs:'a array) (l:int):
  0 <= l <= size xs =>
  (take l xs || drop l xs) = xs.
proof.
move => Hsize.
apply arrayP; split; first smt(size_append size_take size_drop).
move => k Hksize.
rewrite get_append // size_take //.
case _: (k < l)=> //= k_l.
+ by rewrite get_take 1:#smt:(size_append size_take size_drop) /#.
by rewrite get_drop 1:/# #smt:(size_append size_take size_drop).
qed.

(** all *)
op all (p : 'x -> bool) (xs : 'x array) : bool = all p (ofarray xs) axiomatized by allE.

lemma allP_mem p (xs:'x array):
  all p xs <=> (forall x, mem (ofarray xs) x => p x).
proof. by rewrite allE allP. qed.
    
lemma allP p (xs:'x array):
  all p xs <=>
  (forall i, 0 <= i < size xs => p xs.[i]).
proof.
rewrite allP_mem=> />.
split.
+ move=> + i ge0_i ltsz_i - /(_ xs.[i] _) //.
  by rewrite getE mem_nth 1:-sizeE.
move=> + x /memE x_in_xs.
by move: x_in_xs => /> i ge0_i ltsz_i ->.
qed.

(** cons *)
op (::) (x : 'x) (xs : 'x array) = (mkarray [x]) || xs axiomatized by consE.

lemma size_cons (x:'x) (xs:'x array):
  size (x::xs) = 1 + size xs.
proof. by rewrite consE size_append size_mkarray. qed.
    
lemma get_cons (x:'x) (xs:'x array) (i:int):
  0 <= i <= size xs =>
  (x::xs).[i] = (0 = i) ? x : xs.[i - 1].
proof.
move => hi; rewrite consE.
case (xs = empty) => hcase.
+ have ->: (mkarray [x] || xs) = mkarray [x].
  + by apply arrayP; smt(size_append size_mkarray get_append).
  move : hi; rewrite hcase size_empty.
  move => [ge0_i gei_0].
  by rewrite (lez_anti i 0) //= getE ofarrayK.
case (i <= 0)=> eq0_i.
+ rewrite (lez_anti i 0) 1:/# get_append_left 1:size_mkarray //=.
  by rewrite getE ofarrayK.
by rewrite get_append_right size_mkarray /#.
qed.

lemma cons_nonempty (x:'x) (xs:'x array):
  x::xs <> empty.
proof.
rewrite -negP=> /(congr1 Array.size).
by rewrite size_cons size_empty; smt(size_ge0).
qed.

lemma consI (x y:'x) (xs ys:'x array):
  x::xs = y::ys <=> x = y /\ xs = ys.
proof.
split=> [|[-> ->] //].
rewrite !arrayP=> - [] len val.
split. 
+ have := val 0 _; first smt(size_cons size_ge0).
  by rewrite !get_cons //= 1,2:size_ge0.
have := len. rewrite 2!size_cons.
move=> /addzI -> //= i /> ge0_i ltsz_i.
have := val (i + 1) _; first smt(size_cons size_ge0).
rewrite !get_cons //=; 1,2:smt(size_cons size_ge0).
smt().
qed.

(** sub *)
op sub (xs : 'x array) (i : int) (j : int) : 'x array = take j (drop i xs) axiomatized by subE.

lemma size_sub (xs:'x array) (s l:int):
  0 <= s => 0 <= l => s + l <= size xs =>
  size (sub xs s l) = l.
proof.
move=> ge0_s ge0_l lesz_sl; rewrite subE size_take //.
by rewrite size_drop /#.
qed.

lemma get_sub (xs:'x array) (s l i:int):
  0 <= s => 0 <= l => s + l <= size xs =>
  0 <= i < l =>
  (sub xs s l).[i] = xs.[i + s].
proof.
move=> /> ge0_s ge0_l lesz_sl ge0_i ltl_i; rewrite subE get_take //.
+ by rewrite size_drop /#.
by rewrite get_drop /#.
qed.

lemma sub_complete (xs : 'x array) (l : int):
  l = size xs => 
  sub xs 0 l = xs.
proof.
move=> />; apply arrayP; split.
+ by rewrite size_sub 2:#smt:(size_ge0).
move=> /> i ge0_i; rewrite size_sub 2:#smt:(size_ge0)=> // ltsz_i.
by rewrite get_sub 2:#smt:(size_ge0).
qed.

(** snoc *)
op (:::) (xs : 'x array) (x : 'x) : 'x array = xs || (mkarray [x]) axiomatized by snocE.

lemma size_snoc (x:'x) (xs:'x array):
  size (xs:::x) = size xs + 1.
proof. by rewrite snocE size_append size_mkarray. qed.

lemma get_snoc (xs:'x array) (x:'x) (i:int):
  0 <= i <= size xs =>
  (xs:::x).[i] = (i < size xs) ? xs.[i] : x.
proof.
move=> /> ge0_i ltsz_i.
rewrite snocE get_append 1:size_append 1:size_mkarray 1:ltzS // ge0_i.
by move: ltsz_i=> /lez_eqVlt [] />; rewrite getE ofarrayK.
qed.  

lemma snoc_nonempty (xs:'x array, x:'x):
  xs:::x <> empty.
proof.
rewrite -negP=> /(congr1 Array.size).
by rewrite size_snoc size_empty #smt:(size_ge0).
qed.

lemma array_ind_snoc (p:'x array -> bool):
  p empty =>
  (forall x xs, p xs => p (xs:::x)) =>
  (forall xs, p xs).
proof.
move=> p0 prec.
apply: arrayW.
elim/last_ind=> />.
+ by rewrite -/empty.
move=> s x ps; move: (prec x (mkarray s) ps).
have -> //: mkarray s ::: x = mkarray (rcons s x).
apply: arrayP=> />.
rewrite size_snoc !size_mkarray size_rcons //=.
move=> i ge0_i /ltzS lesz_i.
rewrite get_snoc size_mkarray //.
by rewrite !getE !ofarrayK nth_rcons /#.
qed.

(** blit *)
op blit (dst : 'x array) (doff : int) (src : 'x array) (soff : int) (l : int) : 'x array =
  (take doff dst) || (sub src soff l) || (drop (doff + l) dst)
axiomatized by blitE.

lemma size_blit (dst src:'x array) (dOff sOff l:int):
  0 <= dOff => 0 <= sOff => 0 <= l =>
  dOff + l <= size dst =>
  sOff + l <= size src =>
  size (blit dst dOff src sOff l) = size dst.
proof.
move=> ge0_doff ge0_soff ge0_l doff_l soff_l.
by rewrite blitE !size_append #smt:(size_take size_drop size_sub).
qed.
    
lemma get_blit (dst src:'x array) (dOff sOff l i:int):
  0 <= dOff => 0 <= sOff => 0 <= l =>
  dOff + l <= size dst =>
  sOff + l <= size src =>
  0 <= i < size dst =>
  (blit dst dOff src sOff l).[i] =
    (dOff <= i < dOff + l) ? src.[i - dOff + sOff]
                           : dst.[i].
proof.  
move => ge0_dOff ge0_sOff ge0_l gedOffl_size gesOffl_size [ge0_i gi_size].
rewrite blitE.
case (i < dOff) => hc.
+ by rewrite get_append_left 1:size_take // 1:/# get_take //#.
rewrite get_append_right 2:size_take 2:/#.
+ by rewrite size_take 2:size_append 2:size_sub // 2:size_drop /#.
case (i < dOff + l) => hc2.
+ by rewrite get_append_left 2:get_sub 1:size_sub /#.
by rewrite get_append_right 2:get_drop ?size_sub ?size_drop /#.
qed.

(** has *)
op has (p : 'a -> bool) xs = has p (ofarray xs).

lemma hasP p (s : 'a array): has p s <=> (exists x, 0 <= x < size s /\ p s.[x]).
proof.
rewrite /has hasP; split=> />.
+ by move=> x; rewrite memE=> /#.
by move=> i ge0_i gti_size p_x; exists s.[i]; rewrite memE /#.
qed.

lemma hasPn (p : 'a -> bool) s :
  !has p s <=> (forall x, 0 <= x < size s => !p s.[x]).
proof. by rewrite hasP negb_exists /#. qed.

(** find *)
op find (p : 'a -> bool) xs = find p (ofarray xs).

lemma find_ge0 p (s : 'a array): 0 <= find p s.
proof. by rewrite /find find_ge0. qed.

lemma has_find p (s : 'a array): has p s <=> (find p s < size s).
proof. by rewrite /has /find has_find sizeE. qed.

lemma find_size p (s : 'a array): find p s <= size s.
proof. by rewrite /find sizeE find_size. qed.

(*lemma find_cat p (s1 s2 : 'a array):
  find p (s1 || s2) = if has p s1 then find p s1 else size s1 + find p s2.
proof. rewrite appendE. smt tmo=30. qed.*)

lemma nth_find p (s : 'a array): has p s => p (s.[(find p s)]).
proof. by rewrite /has /find getE=> /(nth_find witness). qed.

lemma before_find p (s : 'a array) i :
  0 <= i < find p s => ! p (s.[i]).
proof. by rewrite /find getE=> /(before_find witness). qed.

(** equalities *)
lemma list_array_eq (l : 'a list) (a : 'a array) :
        a = mkarray l
    <=> (   size l = size a
         /\ (forall i, 0 <= i < size a => nth witness l i = a.[i])).
proof.   
rewrite arrayP size_mkarray eq_sym.
apply: andb_id2l=> /> eq_size; rewrite eq_iff.
by apply: forall_iff=> /= i; rewrite !getE ofarrayK /#.
qed.

lemma mkarray_ofarray_eq (x : 'a list) (xs : 'a array):
  (mkarray x = xs) <=> (x = ofarray xs).
proof. 
case (size x <> size xs)=> /= [|eq_size].
+ rewrite sizeE; case: (x = ofarray xs)=> /> + _.
  by apply: contraLR=> /= <-; rewrite ofarrayK.
rewrite eq_sym list_array_eq eq_size /=; split=> />.
+ move=> eq_nth; apply: (eq_from_nth witness).
  + by rewrite -sizeE.
  by move=> /> i ge0_i gti_size; rewrite eq_nth 1:-eq_size // getE.
by move=> i; rewrite getE.
qed.

lemma drop_empty (x : int) (xs : 'a array) : xs = empty => drop x xs = empty.
proof.
by move=> ->; rewrite dropE emptyE ofarrayK; by case: (0 <= x).
qed.

lemma array_ind_struct (xs : 'a array) :
  0 < size xs =>
  xs = xs.[0] :: drop 1 xs.
proof.
move=> gt0_size; apply: arrayP=> //=; split.
+ by rewrite size_cons size_drop /#.
move=> /> i ge0_i lti_size; rewrite get_cons.
+ by rewrite size_drop /#.
by case: (i = 0)=> /> i_neq_0; rewrite get_drop /#.
qed.
    
theory Darray.

  import MUnit.
  require DProd.

  op darray (d: 'a distr) (n : int) = dmap (dlist d n) mkarray
  axiomatized by darrayE.

  lemma mu_darray_dlist (d : 'a distr) n xs:
    mu (darray d n) (pred1 xs) = mu (dlist d n) (pred1 (ofarray xs)).
  proof.
  rewrite /darray dmap1E; apply: mu_eq=> x @/(\o) @/pred1.
  exact/eq_iff/mkarray_ofarray_eq.
  qed.

  lemma mu1_darray_dlist (d : 'a distr) n xs:
    mu1 (darray d n) xs = mu1 (dlist d n) (ofarray xs).
  proof. by rewrite mu_darray_dlist. qed.

  lemma darray_support (d : 'a distr) n xs :
    support (darray d n) xs <=> support (dlist d n) (ofarray xs).
  proof.
  rewrite /darray supp_dmap; split=> />.
  + by move=> x; rewrite ofarrayK.
  by move=> ofxs_in_dldn; exists (ofarray xs); rewrite mkarrayK.
  qed.
      
  lemma darray_support0 (d : 'a distr) n xs:
    n <= 0 =>
    support (darray d n) xs <=> xs = empty.
  proof.
  move=> le0_n; rewrite darray_support dlist0 // supp_dunit.
  by rewrite eq_sym -mkarray_ofarray_eq emptyE.
  qed.
  
  lemma darray_support_ge0 (d : 'a distr) n xs:
    0 <= n =>
    support (darray d n) xs <=> size xs = n /\ all (support d) xs.
  proof. 
  by move=> ge0_n; rewrite darray_support supp_dlist // sizeE /all.
  qed.
  
  import StdBigop.Bigreal.BRM.

  lemma mu1_darray (d : 'a distr) n xs:
    0 <= n =>
    mu1 (darray d n) xs
    = if   n = size xs
      then big predT (fun x => mu1 d x) (ofarray xs)
      else 0%r.
  proof.
  by move=> ge0_n; rewrite mu1_darray_dlist dlist1E // sizeE.
  qed.

  lemma mu1_darray_eq (d : 'a distr) (s1 s2 : 'a array):
    s1 = s2 =>
    mu1 (darray d (size s1)) s1 = mu1 (darray d (size s2)) s2.
  proof. by rewrite !mu1_darray 1,2:size_ge0. qed.

  lemma mu_neg (len:int) (d:'a distr) (p:'a array -> bool):
    len < 0 =>
    mu (darray d len) p = b2r (p empty).
  proof. by move=> gt0_len; rewrite darrayE dmapE dlist0E 1:/# emptyE. qed.

  lemma mu1_neg (len:int) (d:'a distr) (x:'a array):
    len < 0 =>
    mu1 (darray d len) x = if x = empty then 1%r else 0%r.
  proof.
  move=> /(mu_neg len d (pred1 x)) -> @/pred1.
  by rewrite (eq_sym empty).
  qed.

  lemma supp_neg (len:int) (d:'a distr) (x:'a array):
    len < 0 =>
    support (darray d len) x <=> x = empty.
  proof. by move=> lt0_len; rewrite darray_support0 1:/#. qed.

  lemma weight_neg (len:int) (d:'a distr):
    len < 0 =>
    weight (darray d len) = 1%r.
  proof. by move=> lt0_len; rewrite mu_neg /#. qed.

  (* Non-negative length case *)
  lemma mu1_def (d:'a distr) (x:'a array):
    mu1 (darray d (size x)) x = foldr (fun x p, p * mu1 d x) 1%r (ofarray x).
  proof. 
  rewrite mu1_darray 1:#smt:(size_ge0) //= /big.
  rewrite filter_predT foldr_map /=.
  by congr; apply: fun_ext=> x'; apply: fun_ext=> z /#.
  qed.

  lemma supp_full (d:'a distr) (x:'a array):
    (forall y, support d y) =>
    support (darray d (size x)) x.
  proof.
  move: (darray_support_ge0 d (size x) x _); first exact: size_ge0.
  by move=> //= ->; rewrite allP=> support_d i _; exact (support_d x.[i]).
  qed.
    
  lemma supp_len (len:int) (x: 'a array) (d:'a distr):
    0 <= len =>
    support (darray d len) x => size x = len.
  proof. by move=> /(darray_support_ge0 d _ x) ->. qed.

  lemma supp_k (len:int) (x: 'a array) (d:'a distr) (k:int):
    0 <= k < len =>
    support (darray d len) x =>
    support d x.[k].
  proof.
  move=> /> ge0_k ltlen_k; rewrite darray_support_ge0 1:/#.
  by move=> />; rewrite allP=> ->.
  qed.
  
  lemma weight_darray_dlist (d : 'a distr) len: weight (darray d len) = weight (dlist d len).
  proof. by rewrite darrayE dmapE (mu_eq _ (predT \o mkarray) (predT)). qed.

  lemma darray_ll (d : 'a distr) n: is_lossless (darray d n) <=> is_lossless (dlist d n).
  proof. by rewrite !/is_lossless weight_darray_dlist. qed.
  
  lemma darrayL (d:'a distr) len:
    0 <= len =>
    weight d = 1%r =>
    weight (darray d len) = 1%r.
  proof.
  rewrite -/(is_lossless (darray d len)) darray_ll /is_lossless.
  by move=> /(weight_dlist d) -> ->; rewrite RField.expr1z.
  qed.

(*
   (* if len is negative, then uniformity is easy to prove.
     otherwise, the folded function can be replaced with the same constant for x and y
     (but we need to know that it is only applied to elements of d's support,
      which justifies leaving it as an axiom for now) *)
  axiom darray_uniform (d:'a distr) len:
    is_uniform d =>
    is_uniform (darray d len).
*)
end Darray.

import StdBigop.Bigreal.BRM Darray.

lemma mkarray_map (xs : 'a list) (f : 'a -> 'b) : mkarray (map f xs) = map f (mkarray xs).
proof. by rewrite /map ofarrayK; reflexivity. qed.

(** 
  The probability of sampling the array (x::xs) is the 
  probability of sampling xs times the probability of 
  sampling x.
*)
lemma mu_x_cons (x:'a) (xs:'a array) (d:'a distr):
 mu1 (darray d (1 + size xs)) (x::xs)
 = (mu1 d x) * (mu1 (darray d (size xs)) xs). 
proof.
rewrite consE !mu1_darray_dlist !dlist1E 1,2:#smt:(size_ge0).
rewrite -!sizeE size_append size_mkarray /=.
by rewrite appendE !ofarrayK /= big_cons {1}/predT.
qed.

require import DProd.

(**
  The probability distribution of an array of size 'len+1' is the 
  product between the probability distribution of the elements of
  the array and the probability distribution of an array of size
  'len'.
*)
axiom darray_cons (d:'a distr) (len:int):
 0 <= len =>
 Darray.darray d (1+len) = dapply (fun x,fst x::snd x) (d `*` (Darray.darray d len)).
    
(**
  The probability of sampling an array of type (xs || ys) is the
  product between the probability of sampling xs and the probability
  of sampling ys.
*)
lemma mu_x_app (x:'a) (xs ys:'a array) (d:'a distr):
  mu1 (darray d (size xs + size ys)) (xs || ys) 
  = (mu1 (darray d (size xs)) xs) * (mu1 (darray d (size ys)) ys).
proof.
rewrite !mu1_darray_dlist !dlist1E 1..3:#smt:(size_ge0).
rewrite -!sizeE size_append //=.
by rewrite appendE ofarrayK big_cat.
qed.

(**
   Sampling an array with size len is equivalent to sampling a bigger array 
   and then cutting him to size len.
*)
theory DArrayTake.
 type t.
 module M = {
  proc gen1(len:int, d: t distr) : t array = {
    var xs: t array;
    xs <$ darray d len;
    return xs;
  }
  proc gen2(len' len:int, d: t distr) : t array = {
    var xs: t array;
    xs <$ darray d len';
    xs <- take len xs;
    return xs;
  }
 }.

 axiom darray_take_equiv:
  equiv [ M.gen1 ~ M.gen2 : 
         ={len, d} /\ 0 <= len{2} <= len'{2} ==> ={res} ].
end DArrayTake.

(**
  Sampling values individually and then assigning them to
  array positions is equivalent to sampling the entire array at one time.
*)
theory DArrayWhile.
 type t.
 module M = {
  proc gen1(len:int, d: t distr, dflt: t) : t array = {
    var i: int;
    var x: t;
    var xs: t array;
    i <- 0;
    xs <- offun (fun k, dflt) len;
    while (i < len) {
      x <$ d;
      xs.[i] <- x;
      i <- i+1;
    }
    return xs;
  }
  proc gen2(len:int, d: t distr) : t array = {
    var xs: t array;
    xs <$ darray d len;
    return xs;
  }
 }.

 axiom darray_loop_equiv:
  equiv [ M.gen1 ~ M.gen2 : 
         ={len, d} /\ 0 <= len{1} ==> ={res} ].
end DArrayWhile.

(**
  Sampling two values and then assigning to an array the output
  of a function that takes them as input is equivalent to generating two
  arrays and then map the function between the two arrays.
*)
theory DArrayWhile2.
 type t1, t2, t.
 module M = {
  proc gen1(len:int,d1:t1 distr,d2:t2 distr,f:t1->t2->t,dflt:t) : t array = {
    var i: int;
    var x: t1;
    var y: t2;
    var z: t array;
    i <- 0;
    z <- offun (fun k, dflt) len;
    while (i < len) {
      x <$ d1;
      y <$ d2;
      z.[i] <- f x y;
      i <- i+1;
    }
    return z;
  }
  proc gen2(len:int,d1:t1 distr,d2:t2 distr,f:t1->t2->t) : t array = {
    var x: t1 array;
    var y: t2 array;
    var z: t array;
    x <$ darray d1 len;
    y <$ darray d2 len;
    z <- offun (fun k, f x.[k] y.[k]) len;
    return z;
  }
 }.

 axiom darray2_loop_equiv:
  equiv [ M.gen1 ~ M.gen2 : 
         ={len, d1, d2, f} /\ 0 <= len{1} ==> ={res} ].

end DArrayWhile2.
